# ecologital_app
# how to deploy 
    open a bash (cmd)
    clone the project from the repo (git clone https://gitlab.com/Piyake/ecologital_app.git)
    navigate root folder of the project 
    then run "flutter pub get"  to get the dependancy from google
    and after that run "Flutter build --release" to build application 
    after the the build complete your application can be found at "ecologital_app/build/app/outputs/flutter-apk"
    it will named as app   -release.apk
# about

    The project uses provider to manage the state of the application. 
    The reason to pick provider is It have very clear and faster approach while development
    Flutter team also recommend the provider packege to use in small to large scale applications 

# contact
    If there any issue You can contact me 
    Email : piyakepasanjith@gmail.com
    TP: +94 710 668 779