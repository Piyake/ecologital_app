import 'package:ecologital_app/provider/cart_provider.dart';
import 'package:ecologital_app/provider/product_provider.dart';
import 'package:flutter/material.dart';
import 'package:provider/provider.dart';
import 'routes/router.dart' as router;

void main() {
    runApp(
        MultiProvider(
            providers:[
                ChangeNotifierProvider<ProductProvider>(create: (_)=>ProductProvider()),
                ChangeNotifierProvider<CartProvider>(create: (_)=>CartProvider()),
            ],
            child: MyApp(),
        )
    );
}

class MyApp extends StatelessWidget {
    @override
    Widget build(BuildContext context) {
      return MaterialApp(
            title: 'Flutter Demo',
            theme: ThemeData(
                    primarySwatch: Colors.blue,
            ),
            initialRoute:'/',
            onGenerateRoute:router.generateRoute,
        );
    }
}

