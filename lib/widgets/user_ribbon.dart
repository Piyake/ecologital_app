import 'package:ecologital_app/mixins/decoration_mixin.dart';
import 'package:ecologital_app/widgets/profile_image.dart';
import 'package:flutter/material.dart';

class UserRibbon extends StatelessWidget with DecoMixin {
    const UserRibbon({ Key? key }) : super(key: key);

    @override
    Widget build(BuildContext context)  {
        return Container(
            child: Row(
                mainAxisAlignment: MainAxisAlignment.spaceBetween,
                crossAxisAlignment: CrossAxisAlignment.center,
                children:[
                    Text(
                        'BLISS',
                        style:Theme.of(context).textTheme.headline4!.copyWith(
                            fontWeight: FontWeight.bold,
                            color: Colors.blueAccent[200]
                        ) ,
                    ),
                    Row(
                        children: [
                            Container(
                                padding:EdgeInsets.symmetric(horizontal: 6),
                                child: Column(
                                    mainAxisAlignment: MainAxisAlignment.center,
                                    crossAxisAlignment: CrossAxisAlignment.end,
                                    children: [
                                        Container(
                                            child: Text(
                                                'Hello,',
                                                style:Theme.of(context).textTheme.subtitle2!.copyWith(
                                                    fontWeight: FontWeight.bold,
                                                    color: Colors.grey
                                                ) ,
                                            ),
                                        ),
                                        Container(
                                            child: Text(
                                                'Sudesh Kumara',
                                                style:Theme.of(context).textTheme.subtitle1!.copyWith(
                                                    fontWeight: FontWeight.bold,
                                                    color: Colors.grey[600]
                                                ) ,
                                            ),
                                        ),
                                    ],
                                ),
                            ),
                            ProfileImage(),
                        ],
                    )
                ]
            ),
        );
    }
}