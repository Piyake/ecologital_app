import 'package:ecologital_app/mixins/decoration_mixin.dart';
import 'package:ecologital_app/model/product_model.dart';
import 'package:ecologital_app/provider/cart_provider.dart';
import 'package:flutter/material.dart';
import 'package:provider/provider.dart';

class CartCard extends StatefulWidget {
    final int index;

  const CartCard({required this.index}); 

    @override
    _CartCardState createState() => _CartCardState();
}

class _CartCardState extends State<CartCard> with DecoMixin{
    @override
    Widget build(BuildContext context) {
        return Consumer<CartProvider>(
            builder: (context,cartProvider,child){
                ProductModel model = cartProvider.cart[widget.index];
                return Container(
                    padding: cardPadding(),
                    margin: cardMargin(),
                    decoration: cardDeco(context),
                    child: Row(
                        crossAxisAlignment: CrossAxisAlignment.start,
                        children: [
                            Container(
                                width:60,
                                child: Image.network(model.image)
                            ),
                            SizedBox(width: 10,),
                            Expanded(
                              child: Column(
                                  mainAxisAlignment: MainAxisAlignment.center,
                                  crossAxisAlignment: CrossAxisAlignment.start,
                                  children: [
                                      Text(
                                          model.model,
                                          style: Theme.of(context).textTheme.headline5,
                                      ),
                                      SizedBox(height: 10,),
                                      Text(
                                          'Rs ${model.price.toString()}',
                                          style: Theme.of(context).textTheme.headline6,
                                      )
                                  ],
                              ),
                            ),
                            InkWell(
                                child: Icon(Icons.close),
                                onTap: (){
                                    cartProvider.remove(index:widget.index);
                                },
                            )
                        ],
                    ),
                );
            }
        );
    }
}