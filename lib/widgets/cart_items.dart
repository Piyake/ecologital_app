import 'package:ecologital_app/provider/cart_provider.dart';
import 'package:ecologital_app/widgets/cart_card.dart';
import 'package:flutter/material.dart';
import 'package:provider/provider.dart';

class CartItems extends StatefulWidget {
  const CartItems({ Key? key }) : super(key: key);

  @override
  _CartItemsState createState() => _CartItemsState();
}

class _CartItemsState extends State<CartItems> {
    double total = 0;
    @override
    Widget build(BuildContext context) {
        return Consumer<CartProvider>(
            builder: (context,cartProvider,child){
                return Container(
                    margin: EdgeInsets.only(top:25),
                    child: Column(
                        children: [
                            ListView.builder(
                                shrinkWrap: true,
                                itemCount: cartProvider.cart.length,
                                itemBuilder: (context, index){
                                    total+=cartProvider.cart[index].price;
                                    return CartCard(
                                        index:index
                                    );
                                }
                            ),
                            Divider(
                                height: 12,
                                thickness: 2,
                                color: Colors.blueAccent[200],
                            ),
                            Container(
                                padding: EdgeInsets.only(top:25),
                                child: Row(
                                    mainAxisAlignment: MainAxisAlignment.spaceBetween,
                                    children: [
                                        Text(
                                            'Total',
                                            style: Theme.of(context).textTheme.headline5!.copyWith(color: Colors.grey[600],),
                                        ),
                                        Text(
                                            'Rs ${total.toString()}',
                                            style: Theme.of(context).textTheme.headline5!.copyWith(color: Colors.grey[600],),
                                        )
                                    ],
                                ),
                            )
                        ],
                    ),
                );
            }
        );
    }
}