import 'package:ecologital_app/provider/product_provider.dart';
import 'package:flutter/material.dart';
import 'package:provider/provider.dart';

class CategoryFilter extends StatefulWidget {
  const CategoryFilter({ Key? key }) : super(key: key);

  @override
  _CategoryFilterState createState() => _CategoryFilterState();
}

class _CategoryFilterState extends State<CategoryFilter> {
    @override
    Widget build(BuildContext context) {
        return Container(
            padding:EdgeInsets.only(top:15) ,
            child: Column(
                crossAxisAlignment: CrossAxisAlignment.start,
                children: [
                    Text(
                        'By Category',
                        style: Theme.of(context).textTheme.headline6!.copyWith(
                            color: Colors.grey
                        ),
                    ),
                    Container(
                        padding: EdgeInsets.symmetric(vertical: 10),
                        child: Row(
                            mainAxisAlignment: MainAxisAlignment.spaceBetween,
                            children: [
                                filterButton('guiter','Guiter','guitar',Colors.amber),
                                filterButton('piano','Piano','piano',Colors.blueAccent),
                                filterButton('drum','Drum','drums',Colors.red),
                                InkWell(
                                    child: Container(
                                        child: Icon(Icons.close),
                                    ),
                                    onTap: (){
                                        Provider.of<ProductProvider>(context,listen: false).categoryFilter(category: '');
                                    },
                                )
                            ],
                        ),
                    )
                ],
            ),
        );
    }

    Widget filterButton(image,title,categoryid,color){
        return InkWell(
            child: Container(
                padding: EdgeInsets.all(7),
                decoration: BoxDecoration(
                    color:color,
                    borderRadius: BorderRadius.circular(13)
                ),
                child: Row(
                    children: [
                        Image.asset(
                            'assets/images/$image.png',
                            height:25,
                            color: Colors.white,
                        ),
                        SizedBox(width:10),
                        Text(
                            title,
                            style: TextStyle(
                                color: Colors.white
                            ),
                        )
                    ],
                ),
            ),
            onTap: (){
                Provider.of<ProductProvider>(context,listen: false).categoryFilter(category: categoryid);
            },
        );
    }
}