import 'package:ecologital_app/mixins/decoration_mixin.dart';
import 'package:flutter/material.dart';

class ProfileImage extends StatelessWidget with DecoMixin{
    const ProfileImage({ Key? key }) : super(key: key);

    @override
    Widget build(BuildContext context) {
        return Container(
            height: 50,
            clipBehavior: Clip.antiAlias,
            padding :EdgeInsets.all(5),
            decoration: proPicDeco(),
            child: Image.asset(
                'assets/images/propic.jpg',
                fit: BoxFit.cover,
            ),
        );
    }
}