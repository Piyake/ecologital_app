import 'package:ecologital_app/mixins/decoration_mixin.dart';
import 'package:ecologital_app/model/product_model.dart';
import 'package:ecologital_app/provider/product_provider.dart';
import 'package:ecologital_app/routes/routing_contsants.dart';
import 'package:flutter/material.dart';
import 'package:provider/provider.dart';

class ProductCard extends StatefulWidget {
    final int index;
    final ProductModel model;

  const ProductCard({required this.index, required this.model});

    @override
    _ProductCardState createState() => _ProductCardState();
}

class _ProductCardState extends State<ProductCard> with DecoMixin{
    ProductProvider? pr;

    @override
    Widget build(BuildContext context) {
        pr = Provider.of<ProductProvider>(context,listen: false);
        return InkWell(
            child: Container(
                margin:cardMargin(),
                padding: cardPadding(),
                decoration: cardDeco(context),
                child: Column(
                    children: [
                        Row(
                            mainAxisAlignment: MainAxisAlignment.spaceBetween,
                            children: [
                                Text(
                                    widget.model.model,
                                    style: Theme.of(context).textTheme.headline6!.copyWith(fontWeight:FontWeight.bold),
                                ),
                                InkWell(
                                    child: Icon(
                                        Icons.favorite,
                                        color: widget.model.like?Colors.red[900]:Colors.grey[200],
                                    ),
                                    onTap: (){
                                        pr!.like(index: widget.index);
                                    },
                                )
                            ],
                        ),
                        Container(
                            padding: EdgeInsets.all(15),
                            height: 250,
                            child:Image.network(widget.model.image)
                        )
                    ],
                ),
            ),
            onTap: (){
                Navigator.pushNamed(context, RouteNames.Details,arguments:{"model":widget.model,"index":widget.index});
            },
        );
    }
}