import 'package:ecologital_app/mixins/decoration_mixin.dart';
import 'package:flutter/material.dart';
class SearchBar extends StatefulWidget {
    final Function onSearch;
    final String placeholder;

    const SearchBar({required this.onSearch, required this.placeholder});
    @override
    _SearchBarState createState() => _SearchBarState();
}

class _SearchBarState extends State<SearchBar> with DecoMixin {
    @override
    Widget build(BuildContext context) {
        return Stack(
          children: [
            Container(
                margin: EdgeInsets.symmetric(vertical: 8),
                padding: EdgeInsets.only(top:2,left:15,right: 15,),
                decoration: BoxDecoration(
                    borderRadius: BorderRadius.all(Radius.circular(13)),
                    border: Border.all(color:Colors.black26)
                ),
                child: Column(
                    crossAxisAlignment: CrossAxisAlignment.stretch,
                    children:[
                        Container(
                            height: 40,
                            child: TextField(
                                decoration: InputDecoration(
                                    // icon: Icon(Icons.search,),
                                    contentPadding: EdgeInsets.symmetric(horizontal:10),
                                    border:null,
                                    focusedBorder:OutlineInputBorder(borderSide: BorderSide(color:Colors.transparent)),
                                    enabledBorder:OutlineInputBorder(borderSide: BorderSide(color:Colors.transparent)),
                                    hintText: widget.placeholder,
                                ),
                                style: Theme.of(context).textTheme.bodyText2!.copyWith(
                                    height: 1
                                ),
                                maxLines: 1,
                                minLines: 1,
                                onChanged:  (String text){
                                        widget.onSearch(text);
                                    },
                            ),
                        ),
                    ]
                ),
            ),
            Positioned(
                right: 5,
                top:13,
                child: Container(
                    padding: EdgeInsets.all(5),
                    decoration: BoxDecoration(
                        shape: BoxShape.circle,
                        color: Theme.of(context).primaryColor
                    ),
                    child: Icon(
                        Icons.search,
                        color: Colors.white,
                    ),
                ),
            )
          ],
        );
    }
}