import 'package:ecologital_app/provider/product_provider.dart';
import 'package:ecologital_app/routes/routing_contsants.dart';
import 'package:flutter/material.dart';
import 'package:provider/provider.dart';

class CartButton extends StatefulWidget {
    @override
    _CartButtonState createState() => _CartButtonState();
}

class _CartButtonState extends State<CartButton> {
  @override
  Widget build(BuildContext context) {
    return  Consumer<ProductProvider>(
        builder: (context,productProvider,child){
            return IconButton(
                onPressed: (){
                    Navigator.pushNamed(context, RouteNames.Cart);
                },
                icon: Icon(
                    Icons.shopping_cart_outlined,
                    color:Colors.black38,
                )
            );
        }
    );
  }
}

