
import 'package:ecologital_app/model/product_model.dart';
import 'package:ecologital_app/views/cart_view.dart';
import 'package:ecologital_app/views/details_view.dart';
import 'package:ecologital_app/views/home_view.dart';
import 'package:flutter/material.dart';
import 'routing_contsants.dart';

Route<dynamic> generateRoute(RouteSettings settings){
    switch (settings.name) {
        case RouteNames.Home:
            return MaterialPageRoute(builder: (context)=>HomeView(),settings: settings);    
        case RouteNames.Cart:
            return MaterialPageRoute(builder: (context)=>CartView(),settings: settings);
        case RouteNames.Details:
            Map<String,dynamic> args = settings.arguments as Map<String,dynamic>;
            return MaterialPageRoute(builder: (context)=>DetailsView(model: args['model'] as ProductModel,index: args['index'] as int),settings: settings);
        default:
            return MaterialPageRoute(builder: (context)=>HomeView(),settings: settings);
    }
}
