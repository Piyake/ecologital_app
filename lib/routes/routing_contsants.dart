class RouteNames{
    static const String Home = '/';
    static const String Details = '/details';
    static const String Cart = '/cart';
}
