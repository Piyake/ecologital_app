import 'dart:convert';

import 'package:ecologital_app/model/product_model.dart';
import 'package:http/http.dart' as http;
import 'package:ecologital_app/util/common_consts.dart';

class ProductAPI {
    Future<List<ProductModel>> get() async {
        try {
            var uri = Uri.https(CommonConstants.baseUrl, '/v3/919a0d45-c054-4452-8175-65538e554272');
            final response = await http.get(
                uri,
                headers: {
                    'Content-Type': 'application/json'
                }
            );
            var jsonData =  json.decode(response.body.toString());
            return List.from(jsonData["result"].map((e)=>ProductModel.fromJson(e)));
        }catch(e){
            throw e;
        }
    }

    
}