class ProductModel {
    final String id;
    final String name;
    final String category;
    final String brand;
    final String model;
    final double price;
    final String colour;
    final String weight;
    final String image;
    bool like =false;

    ProductModel.fromJson(Map<String,dynamic> json)
    :   id=json["id"],
        name=json["name"],
        category=json["category"],
        brand=json["brand"],
        model=json["model"],
        price=json["price"],
        colour=json["colour"],
        weight=json["weight"],
        image=json["image"];
        
}