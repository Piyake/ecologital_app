import 'package:ecologital_app/provider/product_provider.dart';
import 'package:ecologital_app/routes/routing_contsants.dart';
import 'package:ecologital_app/widgets/category_filter.dart';
import 'package:ecologital_app/widgets/product_cards.dart';
import 'package:ecologital_app/widgets/searchbar.dart';
import 'package:ecologital_app/widgets/user_ribbon.dart';
import 'package:flutter/material.dart';
import 'package:flutter_spinkit/flutter_spinkit.dart';
import 'package:provider/provider.dart';

class HomeView extends StatefulWidget {
  const HomeView({ Key? key }) : super(key: key);

  @override
  _HomeViewState createState() => _HomeViewState();
}

class _HomeViewState extends State<HomeView> {
    @override
    Widget build(BuildContext context) {
        return Consumer<ProductProvider>(
            builder: (context,productProvider,child){
                return SafeArea(
                    child: Scaffold(
                        floatingActionButton: FloatingActionButton(
                            child: Icon(Icons.shopping_cart_outlined),
                            onPressed: (){
                                Navigator.pushNamed(context, RouteNames.Cart);
                            },
                        ),
                        body: Container(
                            padding: EdgeInsets.only(top:25,right: 20,left: 20),
                            child: Column(
                                children: [
                                    UserRibbon(),
                                    CategoryFilter(),
                                    SearchBar(
                                        placeholder: 'Search Your Model',
                                        onSearch: (text){
                                            productProvider.search(q: text);
                                        },
                                    ),
                                    SizedBox(height: 10),
                                    if(productProvider.loading)
                                        Container(
                                            child: Center(
                                                child: SpinKitThreeBounce(
                                                    color: Colors.blue,
                                                    size:18,
                                                )
                                            ),
                                        )
                                    else
                                        Expanded(
                                            child: ListView.builder(
                                                shrinkWrap: true,
                                                itemCount: productProvider.products.length,
                                                itemBuilder:(context,index){
                                                    return ProductCard(index: index, model: productProvider.products[index]);
                                                } 
                                            ),
                                        )
                                ],
                            ),
                        ),
                    ),
                );
            }
        );
    }
}