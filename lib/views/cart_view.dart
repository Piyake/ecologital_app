import 'package:ecologital_app/provider/cart_provider.dart';
import 'package:ecologital_app/routes/routing_contsants.dart';
import 'package:ecologital_app/widgets/cart_items.dart';
import 'package:flutter/material.dart';
import 'package:provider/provider.dart';

class CartView extends StatefulWidget {
  

  @override
  _CartViewState createState() => _CartViewState();
}

class _CartViewState extends State<CartView> {
    @override
    Widget build(BuildContext context) {
        return Consumer<CartProvider>(
            builder: (context,cartProvider,child){
                return SafeArea(
                    child: Scaffold(
                        body:Container(
                            padding: EdgeInsets.only(top:35,right: 20,left: 20),
                            child: Column(
                                crossAxisAlignment: CrossAxisAlignment.stretch,
                                children: [
                                    Text(
                                        'BLISS',
                                        style:Theme.of(context).textTheme.headline4!.copyWith(
                                            fontWeight: FontWeight.bold,
                                            color: Colors.blueAccent[200]
                                        ) ,
                                    ),
                                    if(cartProvider.cart.length>0)
                                        Expanded(child: CartItems())
                                    else
                                        noItems(),
                                    if(cartProvider.cart.length>0)
                                        ElevatedButton(onPressed: (){}, child: Text('Check out'))
                                    else
                                        ElevatedButton(
                                            child: Text('Browse Items'),
                                            onPressed: (){
                                                Navigator.popUntil(context, (route) => route.settings.name==RouteNames.Home);
                                            }
                                        )
                                    
                                ],
                            ),
                        )
                    ),
                );
            }
        );
    }

    Widget noItems(){
        return Expanded(
            child: Container(
                child: Column(
                    crossAxisAlignment: CrossAxisAlignment.center,
                    mainAxisAlignment: MainAxisAlignment.center,
                    children: [
                        Icon(Icons.add_shopping_cart_sharp,size: 150,color: Colors.blueAccent[200]),
                        Text(
                            'Your cart is currently empty!',
                            style: Theme.of(context).textTheme.headline6!.copyWith(
                                color: Colors.blueAccent[200]
                            ),
                        ),
                    ],
                ),
            ),
        );
    }
}