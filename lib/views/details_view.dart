import 'package:ecologital_app/model/product_model.dart';
import 'package:ecologital_app/provider/cart_provider.dart';
import 'package:ecologital_app/provider/product_provider.dart';
import 'package:ecologital_app/widgets/cart_button.dart';
import 'package:flutter/material.dart';
import 'package:provider/provider.dart';

class DetailsView extends StatefulWidget {
    final int index;
    final ProductModel model;

    const DetailsView({required this.model,required this.index});


  @override
  _DetailsViewState createState() => _DetailsViewState();
}

class _DetailsViewState extends State<DetailsView> {
    @override
    Widget build(BuildContext context) {
        final cartProvider = Provider.of<CartProvider>(context);
        return Consumer<ProductProvider>(
            builder: (context,productProvider,child){
                return  Container(
                    child: SafeArea(
                        child: Scaffold(
                            backgroundColor: Colors.white,
                            appBar: AppBar(
                                leading: IconButton(
                                    onPressed: (){
                                        Navigator.pop(context);
                                    },
                                    icon: Icon(
                                        Icons.arrow_back_ios_new_outlined,
                                        color: Colors.black38,
                                    )
                                ),
                                actions: [
                                    IconButton(onPressed: (){},icon: Icon(Icons.share,color:Colors.black38)),
                                    IconButton(
                                        onPressed: (){
                                            productProvider.like(index: widget.index);
                                        },
                                        icon: Icon(
                                            Icons.favorite,
                                            color: productProvider.products[widget.index].like?Colors.red:Colors.black38,
                                        )
                                    ),
                                    CartButton()
                                ],
                                elevation: 0,
                                backgroundColor: Theme.of(context).cardColor,
                            ),
                            body: Container(
                                child: Column(
                                    crossAxisAlignment: CrossAxisAlignment.center,
                                    children: [
                                        Container(
                                            padding: EdgeInsets.only(top:25,right: 20,left: 20),
                                            height: 350,
                                            child: Image.network(
                                                widget.model.image,
                                            ),
                                        ),
                                        Expanded(
                                            child: Container(
                                                padding:EdgeInsets.only(top:25,right: 20,left: 20),
                                                decoration: BoxDecoration(
                                                    color: Colors.amber[100],
                                                    borderRadius: BorderRadius.only(
                                                        topLeft: Radius.circular(40),
                                                        topRight: Radius.circular(40)
                                                    )
                                                ),
                                                child: Column(
                                                    crossAxisAlignment: CrossAxisAlignment.stretch,
                                                    children: [
                                                        Text(
                                                            widget.model.model,
                                                            style: Theme.of(context).textTheme.headline4!.copyWith(color: Colors.grey[600],fontWeight: FontWeight.bold),
                                                        ),
                                                        detailsRow("Brand", widget.model.brand),
                                                        detailsRow("Price", widget.model.price.toString()),
                                                        detailsRow("Color", widget.model.colour),
                                                        detailsRow("Weight", widget.model.weight),
                                                        SizedBox(height:20),
                                                        ElevatedButton(
                                                            onPressed: (){
                                                                cartProvider.addToCard(model:productProvider.products[widget.index]);
                                                            }, 
                                                            child: Text(
                                                                'Add to Cart'
                                                            )
                                                        )
                                                    ],
                                                ),
                                            ),
                                            
                                        )
                                    ],
                                ),
                            ),
                        ),
                    ),
                );
            }
        );
    }

    Widget detailsRow(title,value){
        return Container(
            padding:EdgeInsets.only(top: 12),
            child: Row(
                children: [
                    Container(width:150, child: Text(title,style: Theme.of(context).textTheme.headline6!.copyWith(color: Colors.grey[600],fontWeight: FontWeight.bold),)),
                    Text(value,style: Theme.of(context).textTheme.headline6!.copyWith(color: Colors.grey[600]))
                ],
            ),
        );
    }
}

