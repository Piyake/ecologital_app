

import 'package:ecologital_app/model/product_model.dart';
import 'package:ecologital_app/services/products_api.dart';
import 'package:flutter/material.dart';
class ProductProvider extends ChangeNotifier{
    bool loading = true;
    List<ProductModel> allProducts=[];
    List<ProductModel> products=[];
    String category='';
    String q='';

    
    ProductProvider(){
        init();
    }

    init()async{
        allProducts = await ProductAPI().get(); 
        filter();
        endLoading();
    }

    startLoading(){
        loading =true;
        notifyListeners();
    }

    endLoading(){
        loading =false;
        notifyListeners();
    }
    
    like({@required index}){
        products[index].like = !products[index].like;
        notifyListeners();
    }

    search({required String q}){
        this.q=q;
        filter();
        notifyListeners();
    }

    categoryFilter({required String category}){
        this.category=category;
        filter();
        notifyListeners();
    }

    filter(){
        products=[];
        allProducts.forEach((element) {
            if((element.category==this.category || this.category=='') && (this.q=='' || element.model.contains(q)))
                products.add(element);
        });
    }
}
