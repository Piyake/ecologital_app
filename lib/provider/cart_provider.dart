

import 'package:ecologital_app/model/product_model.dart';
import 'package:flutter/material.dart';
class CartProvider extends ChangeNotifier{
    List<ProductModel> cart=[];

    addToCard({required ProductModel model}){
        cart.add(model);
        notifyListeners();
    }

    remove({required int index}){
        cart.removeAt(index);
        notifyListeners();
    }

}
