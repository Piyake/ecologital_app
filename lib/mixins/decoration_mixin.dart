import 'package:flutter/material.dart';

class DecoMixin{
    BoxDecoration proPicDeco(){
        return BoxDecoration(
            shape: BoxShape.circle,
            color: Colors.white,
            boxShadow: [
                BoxShadow(
                    color:Colors.black.withOpacity(0.2),
                    spreadRadius:1,
                    blurRadius: 2,
                    offset: Offset(0,2),
                )
            ],
        );
    }

    EdgeInsets cardMargin(){
        return EdgeInsets.only(bottom: 10,left:4, right:4);
    }

    EdgeInsets cardPadding(){
        return EdgeInsets.all(20);
    }

    cardDeco(context,{double radius=13,Color? color}){
        return BoxDecoration(
            color: color??Theme.of(context).cardColor,   
            borderRadius: BorderRadius.all(Radius.circular(radius)),
            boxShadow: [
                BoxShadow(
                    color:Colors.black.withOpacity(0.2),
                    spreadRadius:1,
                    blurRadius: 2,
                    offset: Offset(0,2),
                )
            ],
        );
    }

}